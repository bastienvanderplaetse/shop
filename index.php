<?php session_start() ?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
            <div class="jumbotron">
              <div class="container">
                  <div class="well">Hello, world!</div>
              </div>
          </div>
        <title>Shop en ligne</title>
        <link rel="stylesheet" type="text/css" href="./lib/css/style_pc.css" />
    </head>
    <body>
            <header id="img_header"></header>
            <nav>
                <?php
                if(file_exists("./lib/php/menu.php")) {
                    include("./lib/php/menu.php");
                }
                ?>
            </nav>
            <article id="contenu">
                <?php
                if(!isset($_SESSION['page'])) {
                    $_SESSION['page'] = 'accueil.php';
                }
                if(isset($_GET['page'])) {
                    $_SESSION['page'] = $_GET['page'];
                }
                if(file_exists("./pages/".$_SESSION['page'])) {
                    include("./pages/".$_SESSION['page']);
                } else {
                    echo "File not found";
                }
                ?>
            </article>
            <footer>
                <?php
                if(file_exists("./pages/footer.php")) {
                    include("./pages/footer.php");
                }
                ?>
            </footer>
    </body>
</html>
