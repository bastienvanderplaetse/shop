<h2>Heures d'ouvertures du magasin</h2>
<h3>Acheter online</h3>
<p>Le magasin en ligne est ouvert 7/24</p>
<h3>Retirer ses achats</h3>
<p>Les entrepôts sont ouverts aux jours et aux heures suivantes:</p>
<ul>
    <li>Mardi : 10h - 18h</li>
    <li>Vendredi : 10h - 19h</li>
</ul>