<section>
    <table>
        <caption>Statistiques des articles</caption>
        <tr>
            <th>Article</th>
            <th>Stock</th>
            <th>Prix d'achat</th>
            <th>Vendus</th>
            <th>Prix de vente</th>
        </tr>
        <tr class="rowPair">
            <td class="cellLeft">Mirai Nikki</td>
            <td>10</td>
            <td>10€</td>
            <td>5</td>
            <td>12€</td>
        </tr>
        <tr class="rowImpair">
            <td class="cellLeft">Death Note</td>
            <td>13</td>
            <td>11€</td>
            <td>20</td>
            <td>15€</td>
        </tr>
        <tr class="rowPair">
            <td class="cellLeft">Full Metal Alchimist - Brotherhood</td>
            <td>15</td>
            <td>15€</td>
            <td>30</td>
            <td>18€</td>
        </tr>
        <tr class="rowImpair">
            <td class="cellLeft">Kuroko no basket</td>
            <td>7</td>
            <td>12€</td>
            <td>5</td>
            <td>13€</td>
        </tr>
        <tr class="rowPair">
            <td class="cellLeft">Higurashi no naku koro ni</td>
            <td>25</td>
            <td>18€</td>
            <td>10</td>
            <td>19€</td>
        </tr>
    </table>
</section>
<section>
    <h2>Contact responsable des stocks</h2>
    <p>Juliette : extension 17</p>
</section>
<section>
    <h2>News</h2>
    <p>Le site progresse bien</p>
</section>
<section>
    <h2>YOLOO</h2>
    <p>Paragraphe du yolo</p>
</section>