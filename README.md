#EPEColfontaine V3

Project created for the EPE Colfontaive website 3rd version.

##Contents
1. [Informations](#markdown-header-1-informations)
2. [Features](#markdown-header-2-features)
3. [News](#markdown-header-3-news)
4. [Known bugs](#markdown-header-4-known-bugs)
5. [Contacts](#markdown-header-5-contacts)
6. [Credits & Acknowledgments](#markdown-header-6-credits-acknowledgments)
7. [Copyright](#markdown-header-7-copyright)

## 1. Informations
_Project name :_ EPE Colfontaine V3  
_Creation :_ September 1, 2016, 6:38 pm, by Bastien Vanderplaetse  
_Current Version :_ No version  
_Latest snapshot :_ 16w36a  
_Framework :_ Symfony 3.1.3

## 2. Features
_No feature_

## 3. News
* Project initialization  
See more on [changelog](CHANGELOG.md)

## 4. Known bugs
_No bug_

## 5. Contacts
_Email :_ site@epecolfontaine.be

## 6. Credits & Acknowledgments
_Author :_ Bastien Vanderplaetse  
Thank to E. Mester for the trust given to this project.

## 7. Copyright
© 2016 epecolfontaine.be - All Rights Reserved.